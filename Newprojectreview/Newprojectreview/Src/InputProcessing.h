#pragma once
#include <SDL2\SDL.h>
#include <glm\glm\glm.hpp>
#include <glm/glm/gtx/transform.hpp>
#include "Camera.h"


class InputProcessing
{
public:
	
	InputProcessing();
	
	void HandleInput(Camera& m_camera);
	void KeyboardInput(Camera& m_camera);
	void mouseMovement(Camera& m_camera, float targetyaw, float targetpitch);

	


private:
	
	Camera m_camera;
	SDL_Event Keyevent;
	float Pitch;
	float Yaw;
	float xpos_;
	float ypos_;
	float xoffset_;
	float yoffset_;
};

