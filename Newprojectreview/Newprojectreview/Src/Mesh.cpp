#include "Mesh.h"
#include <vector>
#include <iostream>

Mesh::Mesh() {}
Mesh::Mesh(Vertex* vertices, unsigned int numVertices)
{
	

	m_drawCount = numVertices;
	

	glGenVertexArrays(1, &m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);
	

	

	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> color;
	std::vector<glm::vec2> texCoord;
	std::vector<glm::vec2> texCoord1;
	
	unsigned int indices[] = { 0,1,2,0,2,3,0,1,5,0,5,4,4,5,6,6,7,4,2,1,5,2,5,6,3,2,6,6,7,3,3,0,4,3,4,7};
	
	positions.reserve(numVertices);
	texCoord.reserve(numVertices);
	color.reserve(numVertices);
	texCoord1.reserve(numVertices);

	

	for (unsigned int i = 0; i < numVertices; i++)
	{
		positions.push_back(*vertices[i].GetPos());
		color.push_back(*vertices[i].GetColor());
		texCoord.push_back(*vertices[i].GetTexCoord());
		texCoord1.push_back(*vertices[i].GetTexCoord1());
	
		

	}

	glGenBuffers(NUM_BUFFERS, m_vertexArrayBuffers);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(positions[0]), &positions[0], GL_STATIC_DRAW);
	
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,0, 0);

	glGenBuffers(NUM_ELEMENTS, m_vertexElementBuffers);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vertexElementBuffers[INDICES_VB]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[COLOR_VB]);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(color[0]), &color[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[TEXTURES1_VB]);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(texCoord[0]), &texCoord[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[TEXTURES2_VB]);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(texCoord[0]), &texCoord[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	
	
	


	
}
void Mesh::Draw() {

	glBindVertexArray(m_vertexArrayObject);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	
}

Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &m_vertexArrayObject);
}

