#pragma once

#include <gl\glew.h>
#include <string>
#include "Shader.h"



class Texture
{
public:
	Texture(const std::string& fileName, const std::string& fileName1);
	~Texture();
	void Bind(Shader &m_shader);


private:
	
	enum {
		texture1,
		texture2,
		NUM_TEXTURE

	};

	GLuint m_texture[NUM_TEXTURE];
	Shader* m_shader;
	
};

