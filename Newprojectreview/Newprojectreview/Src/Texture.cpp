
#define STB_IMAGE_IMPLEMENTATION  
#include "Texture.h"
#include <stb_image.h>
#include <cassert>
#include <iostream>



Texture::Texture(const std::string& fileName, const std::string& fileName1)
{
	
	

	int width, height, numComponents;
	unsigned char* imageData = stbi_load(fileName.c_str(), &width, &height, &numComponents, 4);

	if (imageData == NULL)
		std::cerr << "Texture loading failed for texture" << fileName << std::endl;
	
	glGenTextures(NUM_TEXTURE, m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture[texture1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	float borderColor[] = { 0,0,0,1 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	
	
	stbi_image_free(imageData);
	
	

	int width1, height1, numComponents1;
	unsigned char* imageData1 = stbi_load(fileName1.c_str(), &width1, &height1, &numComponents1, 4);

	if (imageData1 == NULL)
		std::cerr << "Texture loading failed for texture" << fileName1 << std::endl;

	glBindTexture(GL_TEXTURE_2D, m_texture[texture2]);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width1, height1, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData1);
	
	
	stbi_image_free(imageData1);
}




Texture::~Texture()
{
	glDeleteTextures(NUM_TEXTURE, m_texture);


}


void Texture::Bind(Shader &m_shader) {
	
	
	unsigned int unit=0;
	assert(unit >= 0 && unit <= 31);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture[texture1]);
	
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_texture[texture2]);
	
}