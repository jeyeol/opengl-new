#pragma once

#define CAMERA_INCLUDED_H

#include <glm\glm\glm.hpp>
#include <glm\glm\gtx\transform.hpp>

struct Camera
{
public:
	Camera() {}
	Camera(const glm::vec3& pos, float fov, float aspect, float zNear, float zFar)
	{
		this->pos = pos;
		this->_fov = fov;
		this->forward = glm::vec3(0.0f, 0.0f, 1.0f);
		this->up = glm::vec3(0.0f, 1.0f, 0.0f);
		this->projection = glm::perspective(fov, aspect, zNear, zFar);
		
	}

	

	inline glm::vec3 GetPosition() {
		return pos; 
	}

	inline glm::vec3 SetPosition(const glm::vec3& newPosition) {

		pos = newPosition;
		return pos;
	}

	inline float GetFOV() {
		return _fov;
	}
	inline float SetFOV(float newFOV) {
		_fov = newFOV;
		return _fov;

	}


	inline glm::vec3 GetForward() {
		return forward;

	}

	inline glm::vec3 SetForward(const glm::vec3& newforward) {

		forward = newforward;
		return forward;


	}


	inline glm::mat4 GetViewProjection() 
	{
		return projection * glm::lookAt(pos, pos + forward, up);
	}


	
protected:
private:
	
	glm::mat4 projection;
	glm::vec3 pos;
	glm::vec3 forward;
	glm::vec3 up;
	float _fov;
	
};
