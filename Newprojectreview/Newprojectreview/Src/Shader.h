#pragma once

#include <string>
#include <GL\glew.h>
#include <glm\glm\glm.hpp>
#include <glm\glm\gtc\type_ptr.hpp>
#include "Transform.h"
#include "Camera.h"
#include "Mesh.h"




class Shader
{
public:
	Shader(const std::string& fileName);
	void Bind();
	int getShader();
	virtual ~Shader();
	void PositionMat();
	void Update(const Transform& transform, Camera& camera, Mesh& mesh);
	
private:
	static const unsigned int NUM_SHADERS = 2;
	GLuint m_program;
	Mesh mesh_;
	glm::mat4 Position;
	GLuint m_shaders[NUM_SHADERS];
	enum
	{

		TRANSFORM_U,
		VIEW_U,
		MODEL_U,
		TEXTURE0_U,
		TEXTURE1_U,
		NUM_UNIFORMS
	};
	
	
	GLuint m_uniforms[NUM_UNIFORMS];

};

