#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Display.h"
#include <SDL2\SDL.h>
#include "Mesh.h"
#include "Shader.h"
#include <stb_image.h>
#include "Texture.h"
#include "Camera.h"
#include "Transform.h"
#include "InputProcessing.h"
#include <glm\glm\glm.hpp>

int main(int argc, char *argv[]) {

	Display display(800, 600, "hello World!");


	Vertex vertices[] = { 
		Vertex(glm::vec3(-0.5, -0.5, -0.5), glm::vec3 (0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		Vertex(glm::vec3(0.5, -0.5, -0.5),glm::vec3(1,0,0), glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, 0.5, -0.5), glm::vec3(0,0,1), glm::vec2(1,1 ), glm::vec2(1, 1)),
		Vertex(glm::vec3(0.5, 0.5, -0.5), glm::vec3(0.5,1,0.5), glm::vec2(1, 1), glm::vec2(1, 1)),
		Vertex(glm::vec3(-0.5, 0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1), glm::vec2(0, 1)),
		Vertex(glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		
		Vertex(glm::vec3(-0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		Vertex(glm::vec3(0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 1), glm::vec2(1, 1)),
		Vertex(glm::vec3(0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 1), glm::vec2(1, 1)),
		Vertex(glm::vec3(-0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 1), glm::vec2(0, 1.0)),
		Vertex(glm::vec3(-0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),

		Vertex(glm::vec3(-0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(-0.5, 0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(1, 1), glm::vec2(1, 1)),
		Vertex(glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),
		Vertex(glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),
		Vertex(glm::vec3(-0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		Vertex(glm::vec3(-0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		
		Vertex(glm::vec3(0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, 0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(1, 1), glm::vec2(1, 1)),
		Vertex(glm::vec3(0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),
		Vertex(glm::vec3(0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		Vertex(glm::vec3(0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),

		Vertex(glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),
		Vertex(glm::vec3(0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(-0.5, -0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		Vertex(glm::vec3(-0.5, -0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),

		Vertex(glm::vec3(-0.5, 0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),
		Vertex(glm::vec3(0.5, 0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(1, 1), glm::vec2(1, 1)),
		Vertex(glm::vec3(0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(1, 0), glm::vec2(1, 0)),
		Vertex(glm::vec3(-0.5, 0.5, 0.5), glm::vec3(0,1,0),  glm::vec2(0, 0), glm::vec2(0, 0)),
		Vertex(glm::vec3(-0.5, 0.5, -0.5), glm::vec3(0,1,0),  glm::vec2(0, 1.0), glm::vec2(0, 1.0)),
		 };


	

	Shader shader("C:\\openglproject\\OpenGL\\OpenGL\\res\\firstshader");

	Texture texture("C:\\openglproject\\OpenGL\\OpenGL\\res\\bricks.jpg", "C:\\openglproject\\OpenGL\\OpenGL\\res\\mage.png");

	Transform transform;
	
	Mesh mesh(vertices, sizeof(vertices) / sizeof(vertices[0]));
	
	Camera camera(glm::vec3(0.0f, 0.0f, -5.0f),55.0f, float(800/600),0.01f, 1000.0f);

	InputProcessing input;

	while (!display.IsClosed())
	{
		display.Clear(0.0f, 0.15f, 0.3f, 1.0f);
		
		shader.Bind();
		
		texture.Bind(shader);
		input.HandleInput(camera);
		shader.Update(transform, camera, mesh);
		
		
		
		
	
		

		display.SwapBuffers();
		
	}
	return 0;


}