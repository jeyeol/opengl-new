#include "InputProcessing.h"
#include <iostream>



InputProcessing::InputProcessing() {
	SDL_INIT_EVERYTHING;

}


void InputProcessing::HandleInput(Camera& m_camera) {

	while (SDL_PollEvent(&Keyevent)) {
		switch (Keyevent.type) {
		case SDL_KEYDOWN:
			KeyboardInput(m_camera);
			break;
		case SDL_KEYUP:

			break;

		case SDL_MOUSEWHEEL:
			if (Keyevent.wheel.y > 0) {

				m_camera.SetPosition((m_camera.GetPosition() + glm::vec3(0, 0, 2)));
				std::cout << "wheel up applied";
				break;
			}
			if (Keyevent.wheel.y < 0) {

				m_camera.SetPosition(m_camera.GetPosition() + glm::vec3(0, 0, 0. - 2));

				std::cout << "wheel down applied";
				break;
			}

		case SDL_MOUSEMOTION:
			if (Keyevent.button.button == SDL_BUTTON_LEFT) {
				std::cout << Keyevent.motion.x << "   " << Keyevent.motion.y << std::endl;
				mouseMovement(m_camera, 0.002*Keyevent.motion.xrel, 0.002*Keyevent.motion.yrel);
			}
			break;
		}

		
	}
}

void InputProcessing::mouseMovement(Camera& m_camera, float targetyaw, float targetpitch) {
	this->Yaw = targetyaw;
	this->Pitch = targetpitch;
	if (Pitch > 89) { Pitch = 89.0f; }
	if (Pitch < -90) { Pitch = -89.0f; }
	if (Yaw > 90) { Yaw = 89.0f; }
	if (Yaw < -90) { Yaw = -89.0f; }
	glm::vec3 newforward;
	newforward.x= cos(glm::radians(Yaw)) * sin(glm::radians(Pitch));
	newforward.y= sin(glm::radians(Pitch));
	newforward.z= sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	newforward = m_camera.SetForward(glm::normalize(newforward));
	
}


void InputProcessing::KeyboardInput(Camera& m_camera) {
	
	switch (Keyevent.key.keysym.sym) {
	case SDLK_UP:
		m_camera.SetPosition(m_camera.GetPosition() + glm::vec3(0, -0.1, 0));
		std::cout << "up arrow pressed";
		break;
	case SDLK_DOWN:
		m_camera.SetPosition(m_camera.GetPosition() + glm::vec3(0, 0.1, 0));
		std::cout << "down arrow pressed";
		break;
	case SDLK_RIGHT:
		m_camera.SetPosition(m_camera.GetPosition() + glm::vec3(0.1, 0, 0));
		std::cout << "right arrow pressed";
		break;
	case SDLK_LEFT:
		m_camera.SetPosition(m_camera.GetPosition() + glm::vec3(-0.1, 0, 0));
		std::cout << "left arrow pressed";
		break;
	}



}