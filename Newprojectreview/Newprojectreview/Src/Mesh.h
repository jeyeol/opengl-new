#pragma once

#include <glm\glm\glm.hpp>
#include <GL\glew.h>
#include <string>
#include <vector>

class Vertex

{
public:
	Vertex(const glm::vec3& pos, const glm::vec3& color, const glm::vec2& texCoord, const glm::vec2& texCood1)
	{
		this->pos = pos;
		this->color = color;
		this->texCoord = texCoord;
		this->texCoord1 = texCood1;
	}
	glm::vec3* GetPos() { return &pos; }
	glm::vec2* GetTexCoord() { return &texCoord; }
	glm::vec2* GetTexCoord1() { return &texCoord1; }
	glm::vec3* GetColor() { return &color; }
	


private:
	glm::vec3 pos;
	glm::vec2 texCoord;
	glm::vec2 texCoord1;
	glm::vec3 color;
	

};



class Mesh
{
public:
	Mesh();
	Mesh(Vertex* vertices, unsigned int numVertices);

	~Mesh();
	void Draw();
	

private:
	enum
	{
		POSITION_VB,
		COLOR_VB,
		TEXTURES1_VB,
		TEXTURES2_VB,
		NUM_BUFFERS
	
	};
	enum indices

	{
		INDICES_VB,
		NUM_ELEMENTS


	};
	GLuint m_vertexArrayObject;
	GLuint m_vertexElementBuffers[NUM_ELEMENTS];
	GLuint m_vertexArrayBuffers[NUM_BUFFERS];
	
	
	

	unsigned int m_drawCount;
	

};
